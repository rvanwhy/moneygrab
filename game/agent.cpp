#include "game.hpp"

Agent::Agent()
{
  //reputationBank = new float[POPULATION]();
  alive = true;
  elo = 1200;
  hfactor = elo / 1200;
  money = (float)TOTAL_MONEY / POPULATION;
  x = rand() % HEIGHT + 1;
  y = rand() % WIDTH + 1;
  win = 0.0;
  loss = 0.0;
}

void Agent::draw()
{
  //Agent size is calculated here
  float pixelw = (float) SCREEN_WIDTH / (float)WIDTH;
  float pixelh = (float) SCREEN_HEIGHT / (float)HEIGHT;
  

  glBegin( GL_QUADS );
  glColor3f(this->money/TOTAL_MONEY*10,this->money/TOTAL_MONEY*10, this->money/TOTAL_MONEY*10);
  glVertex2f(pixelw*this->x,       pixelh*this->y);
  glVertex2f(pixelw*(this->x + 1), pixelh*this->y);
  glVertex2f(pixelw*(this->x + 1), pixelh*(this->y + 1));
  glVertex2f(pixelw*this->x,       pixelh*(this->y + 1));
  glEnd();
  
}

void Agent::speakNiceAbout(Agent *agents, int id)
{
  for (int i = 0; i < POPULATION; i++){
    if (onTopOf(*this, agents[i]))
      continue;
    else if (withinSpeechRadius(*this, agents[i])){
      //agents[i].reputationBank[id] += .5;
    }
      
  }
  
  
}
