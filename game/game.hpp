#ifndef GAME_H
#define GAME_H
#include <iostream>
#include <vector>
#include "SDL/SDL.h"
#include "agent.hpp"
#include "SDL/SDL_opengl.h"
#include <math.h>

/*OpenGl Stuff*/
void init(void);
void init_opengl(void);
void display(void);
void renderAgents(Agent *agents);
bool onTopOf(Agent first, Agent second);
float distanceBetween(Agent first, Agent second);
bool withinSpeechRadius(Agent first, Agent second);


void seed(Agent agents[]);
void print_info(Agent agents[]);
void moneygrab(Agent *agent1, Agent *agent2);
void translate_agents(Agent *agents[]);
bool determine_winner(float probability);
bool compare_agents(Agent agent1, Agent agent2);
void evaluate_hfactor_changes(Agent **agents);
void print_csv(Agent *agents);
#define K 32


/*gl Defines*/
#define SCREEN_WIDTH 600
#define SCREEN_HEIGHT 600
#define SCREEN_DEPTH 32
#define PIXEL_HEIGHT 50
#define PIXEL_WIDTH 50
#define FPS 60

/*game defines*/
#define SIZE 30
#define WIDTH 30
#define HEIGHT 30
/*area = 900units*/
#define TOTAL_MONEY 1000000
#define POPULATION 50


#endif
