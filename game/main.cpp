/**
* Moneygrab
* An experimental simulation
* Randall Van Why
* 01-06-2013
*/
#include "game.hpp"

/*
  The ELO Rating system will be used in this simulation.
  Elo is based on wins/losses of monetary exchanges during
  the simulation's run. Though all agents start at the same elo,
  the adjustments should skyrocket some to high elo and shoot some
  to lower elo. Those at extrema of ELO, when faced with extreme
  wins/losses are more likely to commit crimes to win (or not lose).

  hFactor the agent's likelyhood to commit crime. Tied with elo
  (one must take into account that crime may give one bad rep) which
  would negatively affect h. Magnitudes of crime must be taken into account
*/

enum {MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY};

void seed(Agent *agents)
{
  srand(time(NULL));
  for (int i = 0; i < POPULATION; i++){
    agents[i].id = i;
  }
}

void print_info(Agent *agents)
{
  for (int i = 0; i < POPULATION; i++){
    std::cout << "Agent " <<  i << std::endl;
    printf("This agent is %s\n", agents[i].alive == true ? "alive":"dead");
    std::cout << "Elo: "<< agents[i].elo << std::endl;
    printf("hFactor: %.1f\n", agents[i].hfactor);
    printf("Money: $%.2f\n", agents[i].money);
    printf("Reputation: %.2f\n",/*agents[i].reputationBank[i]*/ 0.0);
  }
}

bool determine_winner(float probability)
{
  return (float)rand()/(float)RAND_MAX < probability;
}

//This is a BIG misapplication of ELO
void moneygrab(Agent *agent1, Agent *agent2)
{
  float win1, win2;
  float aelo1, aelo2;
  float loss;
  
  if (agent1->money > agent2->money)
    loss = agent2->money / 2.0;
  else
    loss = agent1->money / 2.0;



  win1 = 1/(1 + pow(10, ((aelo1 - aelo2)/400.0)));
  win2 = 1/(1 + pow(10, ((agent1->elo - agent2->elo)/400.0)));

  if (determine_winner(win1)){
    agent1->money += loss;
    agent2->money -= loss;
    agent1->win += 1;
    agent2->loss += 1;
  }else{
    agent2->money += loss;
    agent1->money -= loss;
    agent2->win += 1;
    agent1->loss += 1;    
  }

  agent1->elo += K*(agent1->win / (agent1->win + agent1->loss) - win2);
  agent2->elo += K*(agent2->win / (agent2->win + agent2->loss) - 1 - win2);  
}


void translate_agents(Agent **agents)
{
  for (int i = 0; i < POPULATION; i++){
    (*agents)[i].x = (WIDTH + ((*agents)[i].x + (rand()%3)-1)) % WIDTH;
    (*agents)[i].y = (WIDTH + ((*agents)[i].y + (rand()%3)-1)) % HEIGHT;
  }
}

void evaluate_hfactor_changes(Agent **agents)
{
  //Blank for now, while we rework this
}

bool compare_agents(Agent agent1, Agent agent2)
{
  if (agent1.x == agent2.x && agent1.y == agent2.y)
    return true;

  return false;
}

void print_csv(Agent *agents)
{
  FILE *fp;
  fp = fopen("out.csv", "w");
  fprintf(fp, "id,alive,elo,money,hfactor,rep\n");
  for (int i = 0; i < POPULATION; i++){
    fprintf(fp,"%d,%s,%d,%f,%f,%f\n",agents[i].id, 
	    agents[i].alive ? "True":"False", agents[i].elo, 
	    agents[i].money, agents[i].hfactor, 0.0/*agents[i].reputationBank[i]*/);
  }
  
}

int main(void)
{
  Agent* agents = new Agent[POPULATION];
  int day;
  bool done = false;
  SDL_Event event;
  seed(agents);
  init();
  init_opengl();
  
  while(!done){
    while(SDL_PollEvent(&event)){			
      if(event.type == SDL_QUIT)
	done = true;
    }

    if(SDL_GetTicks() % 250 == 0){
      day++;
      translate_agents(&agents);
      for (int i = 0; i < POPULATION; i++){
	for (int j = i+1; j < POPULATION; j++){
	  if (compare_agents(agents[i], agents[j]))
	    moneygrab(&agents[i], &agents[j]);
	}
      }
    
      //evaluate_hfactor_changes(&agents);
      renderAgents(agents);
    }
  }
	
  SDL_Quit();
  std::cout << "The games have concluded." << day <<" days have passed...\n";
  std::cout << "Here are the results:" << std::endl;
  print_info(agents);
  print_csv(agents);
  delete agents;
  return 0;
}
