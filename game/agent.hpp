#ifndef AGENT_HPP
#define AGENT_HPP
#include "game.hpp"


class Agent
{
public:
  bool alive;
  float money;
  float hfactor;
  //  float *reputationBank;
  

  int elo;
  float win;
  float loss;

  int x;
  int y;
  int id;

  Agent *mind; //The agent on this agent's mind
  
  Agent();
  void draw();
  void speakNiceAbout(Agent *agents, int id);
};




#endif
